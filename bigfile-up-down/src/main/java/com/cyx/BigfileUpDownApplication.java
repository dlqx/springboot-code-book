package com.cyx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigfileUpDownApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigfileUpDownApplication.class);
    }

}

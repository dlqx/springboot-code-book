package com.cyx.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan("com.cyx.mapper")
public class MybatisPlusConfig {
}

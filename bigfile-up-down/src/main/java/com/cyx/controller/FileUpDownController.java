package com.cyx.controller;

import com.cyx.entity.MultipartFileParam;
import com.cyx.entity.UploadResultVo;
import com.cyx.service.FileUpDownService;
import com.cyx.util.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/file")
public class FileUpDownController {

    @Resource
    private FileUpDownService fileUpDownService;

    @PostMapping("/upload")
    public Result upload(MultipartFileParam param) {
        UploadResultVo vo;
        if (param.getChunkFlag()) {
            vo = fileUpDownService.chunkUpload(param);
        } else {
            vo = fileUpDownService.singleUpload(param);
        }
        return Result.success(vo);
    }

    @GetMapping("/fastCheck")
    public Result fastCheck(@RequestParam(value = "isChunk") Boolean isChunk,
                            @RequestParam(value = "md5") String md5) {
        UploadResultVo vo = fileUpDownService.fastUploadCheck(isChunk, md5);
        return Result.success(vo);
    }
}

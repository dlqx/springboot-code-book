package com.cyx.exception;

import com.cyx.util.MsgConstant;
import com.cyx.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public Result handleException(Exception e) {
        log.error(e.getMessage(), e);
        return Result.error(MsgConstant.SYSTEM_EXCEPTION);
    }

    @ExceptionHandler(CustomException.class)
    public Result businessException(CustomException e) {
        if (null == e.getCode()) {
            return Result.error(e.getMessage());
        }
        return new Result(e.getCode(), e.getMessage());
    }
}

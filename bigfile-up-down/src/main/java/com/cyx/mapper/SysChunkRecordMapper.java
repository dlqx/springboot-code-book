package com.cyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyx.entity.SysChunkRecord;

public interface SysChunkRecordMapper extends BaseMapper<SysChunkRecord> {
}

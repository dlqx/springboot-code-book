package com.cyx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cyx.entity.SysFile;

public interface SysFileMapper extends BaseMapper<SysFile> {
}

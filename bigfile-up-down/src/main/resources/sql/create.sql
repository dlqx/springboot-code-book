CREATE TABLE `sys_file` (
                            `kid` varchar(36) NOT NULL COMMENT 'kid',
                            `file_name` varchar(255) NOT NULL COMMENT '文件名称',
                            `extension` varchar(255) DEFAULT NULL COMMENT '文件扩展名',
                            `file_size` bigint DEFAULT NULL COMMENT '文件大小',
                            `file_path` varchar(255) NOT NULL COMMENT '文件存放路径',
                            `file_md5` varchar(255) NOT NULL COMMENT '文件md5',
                            `remark` varchar(500) DEFAULT NULL COMMENT '备注',
                            `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                            `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
                            PRIMARY KEY (`kid`),
                            UNIQUE KEY `idx_file_md5` (`file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='附件表';

CREATE TABLE `sys_chunk_record` (
                                    `kid` varchar(36) NOT NULL COMMENT 'kid',
                                    `chunk_file_name` varchar(255) NOT NULL COMMENT '文件名称',
                                    `chunk_file_path` varchar(255) NOT NULL COMMENT '文件存放路径',
                                    `file_md5` varchar(255) NOT NULL COMMENT '文件md5',
                                    `chunk_file_md5` varchar(255) NOT NULL COMMENT '文件md5',
                                    `chunk` int NOT NULL COMMENT '第几块分片',
                                    `total_chunk` int NOT NULL COMMENT '总分片数量',
                                    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
                                    PRIMARY KEY (`kid`),
                                    UNIQUE KEY `idx_chunk_file_md5` (`chunk_file_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='附件表';
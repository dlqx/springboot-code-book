package com.cyx.controller;

import com.cyx.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    /**
     * 根据freemarker模板导出user信息-pdf文件
     */
    @GetMapping("/export")
    public void userInfoExport(HttpServletResponse response) throws Exception {
        userService.userInfoExport(response);
    }
}

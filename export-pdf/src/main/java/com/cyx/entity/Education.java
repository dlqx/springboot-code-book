package com.cyx.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Education {
    private String system; // 教育制度
    private String level; // 文化程度
    private String degree; // 学位
    private String university; // 毕业院校
    private String major; // 专业
}

package com.cyx.entity;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
public class User {
    private Integer userId; // 用户账号
    private String userName; // 用户名称
    private String sex; // 性别
    private String photoUrl; // 头像路径
    private LocalDate birthday; // 出生年月日
    private String nationality; // 民族
    private String hometown; // 籍贯
    private String phone; // 联系方式
    private String email; // 邮箱
    private String introduction; // 简介

    private List<Education> educationList;
}

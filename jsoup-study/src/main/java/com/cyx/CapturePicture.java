package com.cyx;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * 捕获图片
 */
public class CapturePicture {
    public static void main(String[] args) throws Exception {
        String url = "https://www.csdn.net/?spm=1018.2226.3001.4476";
        Document document = Jsoup.connect(url).get();
        Elements pics = document.select("img");
        System.out.println(pics);
    }
}

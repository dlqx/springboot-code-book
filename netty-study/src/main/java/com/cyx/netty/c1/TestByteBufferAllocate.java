package com.cyx.netty.c1;

import java.nio.ByteBuffer;

public class TestByteBufferAllocate {
    public static void main(String[] args) {
        System.out.println(ByteBuffer.allocate(16).getClass());
        System.out.println(ByteBuffer.allocateDirect(16).getClass());
        /*
        class java.nio.HeapByteBuffer java堆内存，读写效率较低，会受到GC影响
        class java.nio.DirectByteBuffer 直接内存，读写效率高（少一次数据拷贝），不会受到GC影响，分配内存的效率较低
         */
    }
}

package com.cyx.netty.c1;

import java.nio.ByteBuffer;

public class TestByteBufferRead {
    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put(new byte[]{'a', 'b', 'c', 'd'});
        buffer.flip();

        // 批量读
        buffer.get(new byte[4]);
        // 重头读
        buffer.rewind();
        // mark & reset
        // mark做一个标记，记录position位置，reset是将position重置到mark的位置
        System.out.println(((char) buffer.get()));
        buffer.mark();
        System.out.println(((char) buffer.get()));
        buffer.reset();
        System.out.println(((char) buffer.get()));
        // get(i) 不会改变索引的位置
        System.out.println(((char) buffer.get(2)));
    }
}

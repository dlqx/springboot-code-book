package com.cyx.netty.c1;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class TestByteBufferString {
    public static void main(String[] args) {
        // 1.字符串转为ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(16);
        buffer.put("hello".getBytes());
        buffer.flip();
        out(buffer);

        // 2.Charset 直接切换到读模式
        ByteBuffer buffer1 = StandardCharsets.UTF_8.encode("world");
        out(buffer1);
        // 3.wrap 直接切换到读模式
        ByteBuffer buffer2 = ByteBuffer.wrap("!".getBytes());
        out(buffer2);
    }

    public static void out(ByteBuffer buffer) {
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            System.out.println(((char) b));
        }
    }
}

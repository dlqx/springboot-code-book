package com.cyx.netty.c1;

import org.springframework.core.io.ClassPathResource;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

public class TestGatheringWrites {
    public static void main(String[] args) {
        ByteBuffer b1 = StandardCharsets.UTF_8.encode("hello");
        ByteBuffer b2 = StandardCharsets.UTF_8.encode("world");
        ByteBuffer b3 = StandardCharsets.UTF_8.encode("你好");

        try {
            File file = ResourceUtils.getFile("classpath:words2.txt");
            FileChannel channel = new RandomAccessFile(file, "rw").getChannel();
            channel.write(new ByteBuffer[]{b1, b2, b3}); // 写入的是target下的文件
            channel.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

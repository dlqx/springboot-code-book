package com.cyx.netty.c1;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class TestScatteringReads {
    public static void main(String[] args) {
        try {
            File file = ResourceUtils.getFile("classpath:words.txt");
            FileChannel channel = new RandomAccessFile(file, "r").getChannel();
            ByteBuffer b1 = ByteBuffer.allocate(3);
            ByteBuffer b2 = ByteBuffer.allocate(3);
            ByteBuffer b3 = ByteBuffer.allocate(5);
            channel.read(new ByteBuffer[]{b1, b2, b3});
            b1.flip();
            b2.flip();
            b3.flip();
            out(b1);
            out(b2);
            out(b3);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void out(ByteBuffer buffer) {
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            System.out.println(((char) b));
        }
    }
}

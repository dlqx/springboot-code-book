package com.cyx.netty.c2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Client {
    public static void main(String[] args) throws IOException {
        SocketChannel sc = SocketChannel.open();
        sc.connect(new InetSocketAddress("localhost", 8888));
        // debug
        // Evaluate Expression --> sc.write(Charset.defaultCharset().encode("hello"))
        System.out.println("debug waiting...");
    }
}

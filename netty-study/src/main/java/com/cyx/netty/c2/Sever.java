package com.cyx.netty.c2;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

/*
阻塞方法 线程停止运行 不占用CPU时间
非阻塞模式下一直在循环（不常用），优化方式：Selector
 */
@Slf4j
public class Sever {
    public static void main(String[] args) throws IOException {
        // 使用nio来理解阻塞模式，单线程
        // 0.ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(16);
        // 1.创建服务器
        ServerSocketChannel ssc = ServerSocketChannel.open();
//        ssc.configureBlocking(false); // ServerSocketChannel非阻塞模式
        // 2.绑定监听端口
        ssc.bind(new InetSocketAddress(8888));
        // 3.连接集合
        List<SocketChannel> channels = new ArrayList<>();
        while (true) {
//            log.debug("connecting...");
            // 4.accept 建立与客户端连接，SocketChannel用来与客户端之间通信
            // accept() 默认阻塞方法；非阻塞模式下线程会继续运行，如果没有建立连接，会返回null
            SocketChannel sc = ssc.accept();
            if (sc != null) {
                log.debug("connected... {}", sc);
//                sc.configureBlocking(false); // SocketChannel非阻塞模式
                channels.add(sc);
            }
            for (SocketChannel channel : channels) {
                // 5.接收客户端发送的数据
//                log.debug("before read... {}", channel);
                // read() 默认阻塞方法；非阻塞模式下线程会继续运行，如果没有读到数据，会返回0
                int read = channel.read(buffer);
                if (read > 0) {
                    buffer.flip();
                    out(buffer);
                    buffer.clear();
                    log.debug("after read... {}", channel);
                }
            }
        }
    }

    public static void out(ByteBuffer buffer) {
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            System.out.print(((char) b));
        }
    }
}

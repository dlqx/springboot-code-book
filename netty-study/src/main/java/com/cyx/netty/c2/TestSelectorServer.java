package com.cyx.netty.c2;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;

/*
Selector——
    处理accept、
    cancel、
    处理read、
    用完key要remove、
    处理客户端断开
 */
@Slf4j
public class TestSelectorServer {
    public static void main(String[] args) throws IOException {
        // 1.创建selector，管理多个channel
        Selector selector = Selector.open();
        ServerSocketChannel ssc = ServerSocketChannel.open();
        ssc.configureBlocking(false);
        // 2.建立selector和channel的联系（注册）
        // SelectionKey就是将来事件发生后，通过它可以知道事件是哪个channel的事件
        SelectionKey sscKey = ssc.register(selector, 0, null);
        // key只关注accept事件
        sscKey.interestOps(SelectionKey.OP_ACCEPT);
        log.debug("sscKey: {}", sscKey);
        ssc.bind(new InetSocketAddress(8888));
        while (true) {
            // 3.select方法，没有事件发生，线程阻塞；有事件发生，线程才会恢复运行
            // select在事件未处理时，它不会阻塞，事件发生后要么处理要么取消，不能置之不理
            // key.cancel()方法可以取消 反注册，从selector的keys中删除掉
            selector.select();
            // 4.处理事件，selectedKey内部包含了所有发生的事件
            // selector会在发生事件后，向selectedKeys集合中加入key，但不会删除
            Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
            while (iterator.hasNext()) {
                SelectionKey key = iterator.next();
                // 重要：处理key时，要从selectedKey集合中删除，否则下次处理就会有问题
                iterator.remove();
                log.debug("key: {}", key);
                // 5.区分事件类型
                if (key.isAcceptable()) { // 如果是accept
                    ServerSocketChannel channel = (ServerSocketChannel) key.channel();
                    SocketChannel sc = channel.accept();
                    sc.configureBlocking(false);
                    SelectionKey scKey = sc.register(selector, 0, null);
                    scKey.interestOps(SelectionKey.OP_READ);
                    log.debug("sc: {}", sc);
                    log.debug("scKey: {}", scKey);
                } else if (key.isReadable()) { // 如果是read
                    // 注意：客户端关闭时会引发一个read事件
                    try {
                        SocketChannel channel = (SocketChannel) key.channel();
                        ByteBuffer buffer = ByteBuffer.allocate(16);
                        int read = channel.read(buffer);
                        // 如果是客户端正常断开，read是-1
                        if (read == -1) {
                            key.cancel();
                        } else {
                            buffer.flip();
                            out(buffer);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        key.cancel(); // 因为客户端强制关闭了，因此需要将key移除
                    }
                }
            }
        }
    }

    public static void out(ByteBuffer buffer) {
        while (buffer.hasRemaining()) {
            byte b = buffer.get();
            System.out.print(((char) b));
        }
    }
}

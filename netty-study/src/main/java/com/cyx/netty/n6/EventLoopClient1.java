package com.cyx.netty.n6;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;

import java.net.InetSocketAddress;

/*
使用ChannelFuture
带有Future、Promise的类一般都是和异步，方法配套使用，用来正确处理结果

对应服务端类——EventLoopServer、EventLoopServer1
 */
public class EventLoopClient1 {
    public static void main(String[] args) throws InterruptedException {
        ChannelFuture channelFuture = new Bootstrap()
                .group(new NioEventLoopGroup())
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        ch.pipeline().addLast(new StringEncoder());
                    }
                })
                // 连接到服务器
                // 异步非阻塞，main发起了调用，真正执行connect的是Nio线程
                .connect(new InetSocketAddress("localhost", 8888));

        // 方法1.调用sync()时会阻塞，目的是等待异步事件完成
        // 阻塞当前线程，知道nio线程连接建立完毕
//        channelFuture.sync();
//        // 如果没有sync()，那么channel()拿不到channel，此时channel还未建立成功
//        Channel channel = channelFuture.channel();
//        channel.writeAndFlush("hello world");

        // 方法2.使用addListener（回调对象）方法异步处理结果
        channelFuture.addListener(new ChannelFutureListener() {
            // nio线程连接建立完以后会调用此方法
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                Channel channel1 = future.channel();
                channel1.writeAndFlush("hello world");
            }
        });
    }
}

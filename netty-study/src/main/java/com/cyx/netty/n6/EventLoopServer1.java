package com.cyx.netty.n6;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;

/*
EventLoopServer优化
使用单独EventLoopGroup进行工作细分

例如 ctx.fireChannelRead(msg); // 让消息传递给下一个handler

对应客户端类——EventLoopClient
 */
@Slf4j
public class EventLoopServer1 {
    public static void main(String[] args) {
        // 细分2 创建一个独立的EventLoopGroup
        EventLoopGroup group = new DefaultEventLoopGroup();

        // 服务器基本都是固定写法，不同的通常是handler
        new ServerBootstrap()
                // 为父级（接受器）和子级（客户端）设置EventLoopGroup
                // 第一个参数 只负责ServerSocketChannel上的accept事件
                // 第二个参数 只负责socketChannel上的读写
                .group(new NioEventLoopGroup(), new NioEventLoopGroup(2))
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override // 连接建立后调用
                    protected void initChannel(NioSocketChannel ch) throws Exception {
                        log.info("连接建立...");
                        ch.pipeline().addLast("handler1", new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                ByteBuf buf = (ByteBuf) msg;
                                log.info("out>>> {}", buf.toString(Charset.defaultCharset()));
                                ctx.fireChannelRead(msg); // 让消息传递给下一个handler
                            }
                        }).addLast(group, "handler2", new ChannelInboundHandlerAdapter(){
                            @Override
                            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                ByteBuf buf = (ByteBuf) msg;
                                log.info("out>>> {}", buf.toString(Charset.defaultCharset()));
                            }
                        });
                    }
                })
                .bind(8888);
    }
}

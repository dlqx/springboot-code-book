package com.cyx.netty.n6;

import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.NettyRuntime;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

@Slf4j
public class TestEventLoop {
    public static void main(String[] args) {
        // 1.创建事件循环组
        EventLoopGroup group = new NioEventLoopGroup(); // 既能处理io事件，也能处理普通任务、定时任务
//        EventLoopGroup group1 = new DefaultEventLoopGroup(); // 能处理普通任务、定时任务
        log.info("电脑核心数 {}", NettyRuntime.availableProcessors());

        // 2.获取下一个事件循环对象
        System.out.println(group.next());
        System.out.println(group.next());
        System.out.println(group.next());

        // 3.执行普通任务
        // 意义一：异步执行，部分事件交给事件循环组
        // 意义二：在做事件分发的时候，把接下来一段代码的执行权从一个线程转给另一个线程
        group.next().submit(() -> log.info("ok"));

        // 4.执行定时任务
        // 作用：keepalive 实现连接的保活
        group.next().scheduleAtFixedRate(() -> log.info("hi"), 0, 1, TimeUnit.SECONDS);
    }
}

package com.cyx.netty.n7;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/*
对比jdk的Future、netty的Future和netty的Promise
 */
@Slf4j
public class TestJdkFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        // 1.线程池
        ExecutorService pool = Executors.newFixedThreadPool(2);
        // 2.提交任务
        Future<Integer> future = pool.submit(() -> {
            Thread.sleep(1000);
            return 50;
        });
        // 3.主线程通过future来获取信息
        log.info("结果 {}", future.get());
        pool.shutdown();
    }
}

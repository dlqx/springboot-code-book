package com.cyx.netty.n7;

import io.netty.channel.EventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;

/*
对比jdk的Future、netty的Future和netty的Promise
 */
@Slf4j
public class TestNettyFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        NioEventLoopGroup group = new NioEventLoopGroup();
        EventLoop eventLoop = group.next();
        Future<Integer> future = eventLoop.submit(() -> {
            long time = 1000;
            log.info("time {}", time);
            Thread.sleep(time);
            return 50;
        });
        // 同步方式
//        log.info("result {}", future.get());
        // 异步方式
        future.addListener(future1 -> {
            log.info("result {}", future1.getNow());
            group.shutdownGracefully();
        });
    }
}

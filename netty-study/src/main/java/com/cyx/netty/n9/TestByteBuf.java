package com.cyx.netty.n9;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

import java.nio.charset.StandardCharsets;

/*
byteBuf的容量是可以动态扩容的 ByteBuffer不行

ByteBuf的直接内存和堆内存：
ByteBufAllocator.DEFAULT .heapBuffer() / .directBuffer()

ByteBuf默认使用直接内存，直接内存创建和销毁代价昂贵，但读写性能高（少一次内存复制），适合配合池化功能一起用。

池化的意义在于可以重用ByteBuf。
4.1以后，非Android平台默认启用池化实现，Android平台启用非池化实现。
 */
public class TestByteBuf {
    public static void main(String[] args) {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        System.out.println(buf);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 300; i++) {
            builder.append("A");
        }
        // 自动扩容
        buf.writeBytes(builder.toString().getBytes(StandardCharsets.UTF_8));
        System.out.println(buf);
    }
}

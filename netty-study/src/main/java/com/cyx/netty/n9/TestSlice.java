package com.cyx.netty.n9;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;

/*
ByteBuf 零拷贝
slice
零拷贝的体现之一：对原始ByteBuf进行切片成多个ByteBuf，切片后的ByteBuf并没有发生内存复制，还是使用原始ByteBuf的内存，切片后的ByteBuf维护独立的read、write指针。
 */
public class TestSlice {
    public static void main(String[] args) {
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer(10);
        buf.writeBytes(new byte[]{'a','b','c','d','e','f','g','h','i','j'});
        // 在切片过程中，没有发生数据复制
        ByteBuf buf1 = buf.slice(0, 5);

        buf1.setByte(0, 'z');
        System.out.println(buf.getByte(0));
    }
}
